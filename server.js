'use strict';
// ------ Require the packages
var express = require('express');

// ------ Configure the application
var port = process.env.PORT || 8080;

var server = express();
var api = express.Router();

// ------ Build the routes
api.route('/')
    .get(function(req, res) {
        res.json({
            "message": "Hello, beautiful!"
        });
    });
server.use('/', api);

// ------ Serve the public
server.listen(port, function() {
    console.log("Running on port", port);
});